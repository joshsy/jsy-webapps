$(function() {
  function isValidEmail(email) {
    var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    return re.test(email);
  }

  function isValidPassword(pwd) {
    // at least one number, one lowercase, one uppercase letter, one special symbol
    // at least nine characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}/;
    return re.test(pwd);
  }

  function confirmPassword(pwd, confpwd) {
    // must be the same as password
    return pwd == confpwd;
  }

  function isValidPhone(phone) {
    // phone must be 10 digits
    var re = /^\d{10}$/;
    return re.test(phone);
  }

  function isValidState(state) {
    // state must be two capital letter initial
    var re = /^[A-Z]{2}$/;
    return re.test(state);
  }

  function isValidZipcode(zipcode) {
    // zipcode must be 5 digits
    var re = /^\d{5}$/;
    return re.test(zipcode);
  }

  $('#user-add-form').on('submit', function(event) { // form id

    var firstname = $('#user-firstname-input').val();
    var lastname = $('#user-lastname-input').val();
    var email = $('#user-email-input').val(); // input id
    var pwd = $('#user-password-input').val(); // input id
    var confpwd = $('#user-confpassword-input').val(); //input id
    var address = $('#user-address-input').val();
    var city = $('#user-city-input').val();
    var phone = $('#user-phone-input').val(); // input id
    var state = $('#user-state-input').val(); // input id
    var zipcode = $('#user-zipcode-input').val(); // input id

    if(isValidEmail(email)) {
      $('#email-error').hide(); // div id
    } else {
      $('#email-error').text('Email must be in the correct format.').show();
      event.preventDefault();
    }

    if(isValidPassword(pwd)) {
      $('#password-error').hide(); // div id
    } else {
      $('#password-error').text('Password has to be 9 or more characters, and contain at least 1 upper case, 1 lower case, 1 number, and 1 symbol.').show();
      event.preventDefault();
    }

    if(confirmPassword(pwd, confpwd)) {
      $('#confpassword-error').hide(); // div id
    } else {
      $('#confpassword-error').text('Passwords must match!').show();
      event.preventDefault();
    }

    if(isValidPhone(phone)) {
      $('#phone-error').hide(); // div id
    } else {
      $('#phone-error').text('Phone must be all digits.').show();
      event.preventDefault();
    }

    if(isValidState(state)) {
      $('#state-error').hide(); // div id
    } else {
      $('#state-error').text('State must be two capital letter initials.').show();
      event.preventDefault();
    }

    if(isValidZipcode(zipcode)) {
      $('#zipcode-error').hide(); // div id
    } else {
      $('#zipcode-error').text('Zipcode must be 5 digits.').show();
      event.preventDefault();
    }

    $.ajax(document.URL, {contentType:'application/json', type:'post', data:JSON.stringify({firstname:firstname, lastname:lastname, email:email, password:pwd, phone:phone, address:address, city:city, state:state, zipcode:zipcode})});

  });
});

