''' /users/registration resource for feednd.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Joshua Sy
Date: 26 March 2015
Web Applications'''
import apiutil
from apiutil import errorJSON
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import json
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf
from pyvalidate import validate, ValidationException
from passlib.apps import custom_app_context as pwd_context

class UserRegistration(object):
    ''' Handles resource /users/registration
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        self.db = dict()
        self.db['name']='lectures'
        self.db['user']='root'
        self.db['host']='127.0.0.1'


    def GET(self):
        ''' Prepare user registration page '''

        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        if output_format == 'text/html':
            return env.get_template('userregistration-tmpl.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
            )

#Add above def POST:
    @validate(requires=['firstname', 'lastname', 'email', 'password', 'phone', 'address', 'city', 'state', 'zipcode'],
              types={'firstname':str,'lastname':str, 'email':str, 'password':str, 'phone':str, 'address':str, 'city':str, 'state':str, 'zipcode':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'password':'(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}', 'phone':'^\d{10}$', 'state':'^[A-Z]{2}$', 'zipcode':'^\d{5}$'}
             )
    def check_params(self, firstname, lastname, email, password, phone, address, city, state, zipcode):
              print 'adding user "%s:%s" with firstname: %s:%s email: %s:%s phone: %s:%s' % (lastname, type(lastname),
			 firstname, type(firstname),
                         email, type(email),
                         phone, type(phone))
    @cherrypy.tools.json_in()
    def POST(self):
	firstname = lastname = email = password = phone = address = city = state = zipcode = None
        ''' Add a new user ''' 
        if not firstname:
          try:
            firstname = cherrypy.request.json["firstname"]
            print "firstname received: %s" % firstname
          except:
            print "firstname was not received"
            return errorJSON(code=9003, message="Expected text 'firstname' for user as JSON input")
        if not lastname:
          try:
            lastname = cherrypy.request.json["lastname"]
            print "lastname received: %s" % lastname
          except:
            print "lastname was not received"
            return errorJSON(code=9003, message="Expected text 'lastname' for user as JSON input")
        if not email:
          try:
            email = cherrypy.request.json["email"]
            print "email received: %s" % email
          except:
            print "email was not received"
            return errorJSON(code=9003, message="Expected email 'email' for user as JSON input")
        if not password:
          try:
            password = cherrypy.request.json["password"]
            print "password received"
          except:
            print "password was not received"
            return errorJSON(code=9003, message="Expected password for user as JSON input")
	#confirm password here
        if not phone:
          try:
            phone = cherrypy.request.json["phone"]
            print "phone received: %s" % phone
          except:
            print "phone was not received"
            return errorJSON(code=9003, message="Expected tel 'phone' for user as JSON input")
        if not address:
          try:
            address = cherrypy.request.json["address"]
            print "address received: %s" % address
          except:
            print "address was not received"
            return errorJSON(code=9003, message="Expected address 'address' for user as JSON input")
        if not city:
          try:
            city = cherrypy.request.json["city"]
            print "city received: %s" % city
          except:
            print "city was not received"
            return errorJSON(code=9003, message="Expected city 'city' for user as JSON input")
        if not state:
          try:
            state = cherrypy.request.json["state"]
            print "state received: %s" % state
          except:
            print "state was not received"
            return errorJSON(code=9003, message="Expected state 'state' for user as JSON input")
        if not zipcode:
          try:
            zipcode = cherrypy.request.json["zipcode"]
            print "zipcode received: %s" % zipcode
          except:
            print "zipcode was not received"
            return errorJSON(code=9003, message="Expected zipcode 'zipcode' for user as JSON input")
        try:
            self.check_params(firstname=firstname, lastname=lastname, email=email, password=password, phone=phone, address=address, city=city, state=state, zipcode=zipcode)
        except ValidationException as ex:
            print ex.message
            return errorJSON(code=9003, message=ex.message.encode('ascii','ignore'))

        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor()
        # WARNING: Need to do validation

 #in def POST: after connecting to DB and before INSERT statement:
    # Check if email already exists
        q="SELECT EXISTS(SELECT 1 FROM user WHERE email='%s')" % email
        cursor.execute(q)
        if cursor.fetchall()[0][0]:
            #email already exists
            print "User with email %s Already Exists" % email
            return errorJSON(code=9000, message="User with email %s Already Exists") % email

        hash = pwd_context.encrypt(password)

        q="INSERT INTO user (firstname, lastname, email, password, phone, address, city, state, zipcode) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');" \
            % (firstname, lastname, email, hash, phone, address, city, state, zipcode)
        try:
            cursor.execute(q)
            #userID=cursor.fetchall()[0][0]
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert user
            print "mysql error: %s" % e
            return errorJSON(code=9002, message="Failed to add user")
        result = {'firstname':firstname, 'lastname':lastname, 'email':email, 'password':hash, 'phone':phone, 'address':address, 'city':city, 'state':state, 'zipcode':zipcode, 'errors':[]}
        return json.dumps(result)  
     
        def OPTIONS(self):
            ''' Allows GET, POST, OPTIONS '''
            #Prepare response
            return "<p>/users/registration allows GET, POST, and OPTIONS</p>"
     
class StaticAssets(object):
    pass
     
if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(UserRegistration(), '/users/registration', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(UserRegistration(), None, conf)


