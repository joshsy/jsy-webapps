import cherrypy
import sys
import mysql.connector
import collections

class ExampleApp(object):
    @cherrypy.expose
    def index(self):
        s = """<html>
<head>
        <title>Hungry Scholars</title>
</head>

<body>
	<h2>Hungry Scholars</h2>
	<h4>Run out of Flex Points? Look at these inexpensive and exquisite restaurants to replenish your energy!</h4>

	<ul>
	<li><a href = "">Restaurants</a></li>
	<li><a href = "">Orders</a></li>
	<li><a href = "">Account</a></li>
	</ul>"""

	d = {"Fiddler's Hearth":['127 Main St','574-232-2853', '2.3 miles'], "Rocco's Restaurant":["537 N. St Louis St", "574-232-2464", "1.2 miles"], "Nick's Patio":["1710 N. Ironwood Dr", "574-277-7400", "1.8 miles"]}
	od = collections.OrderedDict(sorted(d.items(), key=lambda t:t[0]))
	for k,v in od.items():
		s += k
		s += '<br>'
		for i1 in v:
			s += i1
			s += '<br>'
		s += '<br><br>'

	s += """</body></html>"""
	return s

    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='root',
                              host='127.0.0.1',
                              database='lectures')
        cursor = cnx.cursor()
        query = ("SELECT name, phone, address, city, state, zip, url from restaurants")
        cursor.execute(query)
#	od = collections.OrderedDict(sorted(query.items(), key=lambda t:t[0]))	
	restdata = ''
	for (Name, Phone, Address, City, State, Zip, Url) in cursor.fetchall():
		restdata += '<b>' + Name + '</b>' + '<br>' + Phone + '<br>' + Address + '<br>' + City + '<br>' + State + '<br>' + Zip + '<br>' + Url + '<br><br>'
	return restdata
application = cherrypy.Application(ExampleApp(), None)
