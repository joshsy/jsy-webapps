#Use this script to populate the |sslp| table of the database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'sslp'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('SSLP_data.json','r')
sslpsiteDict = json.load(inputFile)
inputFile.close()


#Loop through the SSLP Sites and add info to database

cursor.execute("delete from Contact")
cursor.execute("delete from Previous_Students")
cursor.execute("delete from Dates")
cursor.execute("delete from Categories")
cursor.execute("delete from SSLP_Sites")

reset_incr = ("ALTER TABLE SSLP_Sites AUTO_INCREMENT = 1")
cursor.execute(reset_incr)
reset_incr = ("ALTER TABLE Categories AUTO_INCREMENT = 1")
cursor.execute(reset_incr)
reset_incr = ("ALTER TABLE Dates AUTO_INCREMENT = 1")
cursor.execute(reset_incr)
reset_incr = ("ALTER TABLE Previous_Students AUTO_INCREMENT = 1")
cursor.execute(reset_incr)
reset_incr = ("ALTER TABLE Contact AUTO_INCREMENT = 1")
cursor.execute(reset_incr)


	###############################
	## Add SSLP info first ##
	###############################

for key, sslpsite in sslpsiteDict.iteritems():
	
        inputDict = {   #'any variable' : sslpsite.get('json variable'),
		'Name' : key,
		'City' : sslpsite.get('City'),
		'State' : sslpsite.get('State'),
		'AlumClub' : sslpsite.get('Alumni Club'),
		'GenderReq' : sslpsite.get('Gender Requirement'),
		'AgeReq' : sslpsite.get('Minimum Age Requirement'),
		'LangReq' : sslpsite.get('Language Requirement'),
		'DriveLicReq' : sslpsite.get("Driver's License Requirement"),
		'Description' : sslpsite.get('Description'),
                'Transportation' : sslpsite.get('Transportation'),
                'Housing' : sslpsite.get('Housing'),
                'Expectations' : sslpsite.get('Special Expectations')
	}        
	#INSERT INTO table (mysql variables) VALUES (%(any variables)s)
	addsslpsites = ("INSERT INTO SSLP_Sites (Name, City, State, AlumClub, GenderReq, AgeReq, LangReq, DriveLicReq, Description, Transportation, Housing, Expectations) VALUES (%(Name)s, %(City)s, %(State)s, %(AlumClub)s, %(GenderReq)s, %(AgeReq)s, %(LangReq)s, %(DriveLicReq)s, %(Description)s, %(Transportation)s, %(Housing)s, %(Expectations)s)")
	cursor.execute(addsslpsites,inputDict)
        SiteID = cursor.lastrowid
        if sslpsite.has_key('Website'):
                inputDict = {        
                        'SiteID': SiteID,
                        'Type': 'Website',
                        'Contact': sslpsite['Website']
                }
                #print inputDict
        addcontact = ("INSERT INTO Contact (SiteID, Type, Contact) VALUES (%(SiteID)s, %(Type)s, %(Contact)s)")       
	cursor.execute(addcontact,inputDict)
        #SiteID = cursor.lastrowid
        for Categories in sslpsite.get('Site Type'):
		inputDict = {
			'SiteID': SiteID,
			'CategoryName': Categories
		}
		addcategories = ("INSERT INTO Categories (SiteID, CategoryName) VALUES (%(SiteID)s, %(CategoryName)s)")
        	cursor.execute(addcategories,inputDict)
                #SiteID = cursor.lastrowid
        for Dates in sslpsite.get('Dates'):
                inputDict = {
                        'SiteID': SiteID,
                        'StartDate': Dates.split('-')[0],
                        'EndDate': Dates.split('-')[1]
                }
                adddates = ("INSERT INTO Dates (SiteID, StartDate, EndDate) VALUES (%(SiteID)s, STR_TO_DATE(%(StartDate)s, '%m/%d/%Y'), STR_TO_DATE(%(EndDate)s, '%m/%d/%Y'))")
                cursor.execute(adddates,inputDict)
                #SiteID = cursor.lastrowid
        
        for PrevStu, StuInfo in sslpsite.get('Previous Students').iteritems():
                #for StuInfo in PrevStu.iteritems():
                inputDict = {
                        'SiteID': SiteID,
                        'FirstName': PrevStu.split(' ')[0],
                        'LastName': PrevStu.split(' ')[1],
                        'ClassYear': StuInfo['class year'],
                        'SSLPYear': StuInfo['SSLP year'],
                        'email': StuInfo['email']
                }
                #print inputDict   
                addprevstu = ("INSERT INTO Previous_Students (SiteID, FirstName, LastName, ClassYear, SSLPYear, email) VALUES (%(SiteID)s,  %(FirstName)s, %(LastName)s, %(ClassYear)s, %(SSLPYear)s, %(email)s)")
                cursor.execute(addprevstu,inputDict)

cnx.commit()
cnx.close()

