''' /search resource for karinadube.com/SSLPsearch
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Karina Dube
Date: Apr. 20, 2015
Web Applications'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf

class Site(object):
    ''' Handles resource /site
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        #self.id=Search()
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.db['name']='sslp'
        self.db['user']='root'
        self.db['host']='127.0.0.1'


    def _cp_dispatch(self,vpath):
            print "Search._cp_dispatch with vpath: %s \n" % vpath
	    if len(vpath) == 1: # /site/{i}
		cherrypy.request.params['SiteID']=vpath.pop(0)
		print "My parameters: %s" % cherrypy.request.params['SiteID']
            return vpath


    def getDataFromDB(self,SiteID):
	print "Made it here too 9000"
        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
	    results = ""
            cursor = cnx.cursor()

	    q="select SiteId, Name, City, State, GenderReq, description from SSLP_Sites where SiteId=%s;" % SiteID
            print q
	    cursor.execute(q)
	    myResults = cursor.fetchall();
        except Error as e:
            logging.error(e)
            raise
        self.data = []
	self.myd.clear()
	self.xtra.clear()
        for (SiteId, Name, City, State, GenderReq, description) in myResults:
            self.data.append({'Name':Name,
                         'City': City,
                         'State':State,
                         'GenderReq':GenderReq,
                         'description':description,
                         'href':'sites/'+str(SiteId)
                         })
            self.myd[SiteId]=(Name) #edit here. this gets used later to create the dictionary
            self.xtra[SiteId]=(City, State, GenderReq, description) #edit here. gets passed into the renderer for new info.
	    print self.myd
    
    @cherrypy.tools.json_in(force=False)
    def GET(self):
        ''' Get list of SSLP sites '''
        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
        SiteID = cherrypy.request.params.get("SiteID")
        print SiteID
	print "MADE IT HERE %s" % SiteID
	print "SideID = %s" % SiteID
	self.getDataFromDB(SiteID)
        self.sd=dict()
        for key,value in self.myd.iteritems(): #this is where myd is used
           #dist=self.haversine((lat,lng),value)
            self.sd[key]=value
        # Sort by closest restaurant 
        result = OrderedDict(sorted(self.sd.items(), key=lambda t:t[0]))
	print "THIS IS THE RESULT2"
	print result
	if output_format == 'text/html':
            return env.get_template('sites-tmpl.html').render(
                sites=result,
                info=self.xtra, #this is where xtra is used
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

    def POST(self, **kwargs):
        ''' Add a new restaurant '''
        result= "POST /restaurants     ...     Restaurants.POST\n"
        result+= "POST /restaurants body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data; restId should not be included
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/restaurants/ allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Search(), '/restaurants', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Site(), None, conf)

