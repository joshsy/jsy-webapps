#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'sslp'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################


#Site_Favorites
dropTableQuery = ("DROP TABLE IF EXISTS Site_Favorites")
cursor.execute(dropTableQuery)
#Supp_App
dropTableQuery = ("DROP TABLE IF EXISTS Supp_App")
cursor.execute(dropTableQuery)
#Application
dropTableQuery = ("DROP TABLE IF EXISTS Application")
cursor.execute(dropTableQuery)
#Users
dropTableQuery = ("DROP TABLE IF EXISTS Users")
cursor.execute(dropTableQuery)
#Contact
dropTableQuery = ("DROP TABLE IF EXISTS Contact")
cursor.execute(dropTableQuery)
#Previous_Students
dropTableQuery = ("DROP TABLE IF EXISTS Previous_Students")
cursor.execute(dropTableQuery)
#Dates
dropTableQuery = ("DROP TABLE IF EXISTS Dates")
cursor.execute(dropTableQuery)
#Categories
dropTableQuery = ("DROP TABLE IF EXISTS Categories")
cursor.execute(dropTableQuery)
#SSLP_Sites
dropTableQuery = ("DROP TABLE IF EXISTS SSLP_Sites")
cursor.execute(dropTableQuery)


########################
## Create tables next ##
########################

#SSLP_Sites
createTableQuery = ('''CREATE TABLE IF NOT EXISTS SSLP_Sites (
SiteID INT NOT NULL AUTO_INCREMENT,
Name VARCHAR(45) NOT NULL,
City VARCHAR(45) NOT NULL,
State VARCHAR(20) NOT NULL,
AlumClub VARCHAR(20) NOT NULL,
GenderReq VARCHAR(15) NOT NULL,
AgeReq VARCHAR(5) NOT NULL,
LangReq VARCHAR(30) NOT NULL,
DriveLicReq VARCHAR(10) NOT NULL,
Description VARCHAR(1000) NOT NULL,
Transportation VARCHAR(45) NOT NULL,
Housing VARCHAR(45) NOT NULL,
Expectations VARCHAR(1000) NOT NULL,
PRIMARY KEY (SiteID))''')
cursor.execute(createTableQuery)

#Categories
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Categories (
CategoryID INT NOT NULL AUTO_INCREMENT,
CategoryName VARCHAR(20) NOT NULL,
SiteID INT NOT NULL,
INDEX fk_Categories_Sites_idx (SiteID ASC),
PRIMARY KEY (CategoryID),
CONSTRAINT fk_Categories_Sites1
FOREIGN KEY (SiteID)
REFERENCES SSLP_Sites (SiteID)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Dates
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Dates (
DateID INT NOT NULL AUTO_INCREMENT,
StartDate DATE NOT NULL,
EndDate DATE NOT NULL,
SiteID INT NOT NULL,
INDEX fk_Dates_Sites1_idx (SiteID ASC),
PRIMARY KEY (DateID),
CONSTRAINT fk_Dates_Sites1
FOREIGN KEY (SiteID)
REFERENCES SSLP_Sites (SiteID)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Previous_Students
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Previous_Students (
PrevStuID INT NOT NULL AUTO_INCREMENT,
FirstName VARCHAR(20) NOT NULL,
LastName VARCHAR(20) NOT NULL,
ClassYear INT(4) NOT NULL,
SSLPYear INT(4) NOT NULL,
email VARCHAR(45) NOT NULL,
SiteID INT NOT NULL,
PRIMARY KEY (PrevStuID),
INDEX fk_Previous_Students_Sites1_idx (SiteID ASC),
CONSTRAINT fk_Previous_Students_Sites1
FOREIGN KEY (SiteID)
REFERENCES SSLP_Sites (SiteID)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Contact
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Contact (
ContactID INT NOT NULL AUTO_INCREMENT,
Type VARCHAR(20) NOT NULL,
Contact VARCHAR(45) NOT NULL,
SiteID INT NOT NULL,
PRIMARY KEY (ContactID),
INDEX fk_Contact_Sites1_idx (SiteID ASC),
CONSTRAINT fk_Contact_Sites1
FOREIGN KEY (SiteID)
REFERENCES SSLP_Sites (SiteID)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Users
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Users (
NDemail VARCHAR(45) NOT NULL,
FirstName VARCHAR(20) NOT NULL,
LastName VARCHAR(20) NOT NULL,
UserType VARCHAR(20) NOT NULL,
password VARCHAR(120) NOT NULL,
PRIMARY KEY (NDemail));''')
cursor.execute(createTableQuery)

#Application
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Application (
AppID INT NOT NULL AUTO_INCREMENT,
FirstName VARCHAR(20) NOT NULL,
LastName VARCHAR(20) NOT NULL,
Address VARCHAR(100) NOT NULL,
City VARCHAR(45) NOT NULL,
State VARCHAR(2) NOT NULL,
Zipcode INT(5) NOT NULL,
Phone VARCHAR(15) NOT NULL,
ClassYear INT(4) NOT NULL,
Major VARCHAR(20) NOT NULL,
GPA DECIMAL(3,2) NOT NULL,
NDemail VARCHAR(45) NOT NULL,
INDEX fk_Application_Users1_idx (NDemail ASC),
PRIMARY KEY (AppID),
CONSTRAINT fk_Application_Users1
FOREIGN KEY (NDemail)
REFERENCES Users (NDemail)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Supp_App
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Supp_App (
SiteID INT NOT NULL AUTO_INCREMENT,
AppID INT NOT NULL,
AppYear INT(4) NOT NULL,
Question1 VARCHAR(3000) NOT NULL,
Question2 VARCHAR(3000) NOT NULL,
Question3 VARCHAR(3000) NOT NULL,
Question4 VARCHAR(3000) NOT NULL,
Question5 VARCHAR(3000) NOT NULL,
NDemail VARCHAR(45) NOT NULL,
INDEX fk_Supp_App_Application1_idx (AppID ASC),
PRIMARY KEY (SiteID, AppID),
INDEX fk_Supp_App_Users2_idx (NDemail ASC),
CONSTRAINT fk_Supp_App_Application1
FOREIGN KEY (AppID)
REFERENCES Application (AppID)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_Supp_App_Users2
FOREIGN KEY (NDemail)
REFERENCES Users (NDemail)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Site_Favorites
createTableQuery = ('''CREATE TABLE IF NOT EXISTS Site_Favorites (
SiteFavID INT NOT NULL AUTO_INCREMENT,
SiteName VARCHAR(45) NULL,
SiteID INT NOT NULL,
NDemail VARCHAR(45) NOT NULL,
PRIMARY KEY (SiteFavID),
INDEX fk_Site_Favorites_Sites1_idx (SiteID ASC),
INDEX fk_Site_Favorites_Users1_idx (NDemail ASC),
CONSTRAINT fk_Site_Favorites_Sites1
FOREIGN KEY (SiteID)
REFERENCES SSLP_Sites (SiteID)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_Site_Favorites_Users1
FOREIGN KEY (NDemail)
REFERENCES Users (NDemail)
ON DELETE NO ACTION
ON UPDATE NO ACTION);''')
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
