''' Implements handler for /favorites
To add an item to an order '''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from apiutil import errorJSON,  SESSION_KEY
import logging
from config import conf

class Favorites(object):
    ''' Handles resource /orders '''
    exposed = True

    def __init__(self):
        #self.id=Search()
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.db['name']='sslp'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
    
    def _cp_dispatch(self,vpath):
        print "Orders._cp_dispatch with vpath: %s \n" % vpath
        if len(vpath) == 1: # /favorites/{NetID}
            cherrypy.request.params['NetID']=vpath.pop(0)
            return self
        return vpath

    def getDataFromDB(self, NetID):
	print "Made it here too 9000"
        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
	    results = ""
            cursor = cnx.cursor()

            q='select SiteID from Site_Favorites where NDemail="%s"' % (NetID)
	    #q="select SiteId, Name, City, State, GenderReq, description from SSLP_Sites %s;" % results
            print q
	    cursor.execute(q)
	    myResults = cursor.fetchall();
        except Error as e:
            logging.error(e)
            raise	
        self.data = []
	self.myd.clear()
	self.xtra.clear()
	for SiteId in myResults:	    
	    q="select SiteID, Name, City, State, GenderReq, description from SSLP_Sites where SiteID=%s;" % SiteId 
            print q
	    cursor.execute(q)
	    myResults2 = cursor.fetchall();
	    for (SiteId, Name, City, State, GenderReq, description) in myResults2:
        	self.data.append({'Name':Name,
                         	'City': City,
                         	'State':State,
                         	'GenderReq':GenderReq,
                         	'description':description,
                         	'href':'sites/'+str(SiteId)
                         })
            	self.myd[SiteId]=(Name) #edit here. this gets used later to create the dictionary
            	self.xtra[SiteId]=(City, State, GenderReq, description) #edit here. gets passed into the renderer for new info.
	print self.myd


    def GET(self):
        ''' GET orderitemsID, quantity for order_number and itemID or error if no entry with orderID or itemID'''	
        NetID = "abcd"
	output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
	print "MADE IT HERE"
	self.getDataFromDB(NetID)
	self.sd=dict()
        for key,value in self.myd.iteritems(): #this is where myd is used
           #dist=self.haversine((lat,lng),value)
            self.sd[key]=value
        # Sort by closest restaurant 
        result = OrderedDict(sorted(self.sd.items(), key=lambda t:t[0]))
	print "THIS IS THE RESULT2"
	print result
	
	#cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='sslp',charset='utf8mb4')        
        #cursor = cnx.cursor()
        #q='select exists(select 1 from Site_Favorites where NDemail="%s")' % (NetID)
        #cursor.execute(q)
        #if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
        #    return errorJSON(code=9004, message="Favorites for NDemail %s Does Not Exist" % (NetID))
        #q='select SiteName from Site_Favorites where NDemail="%s"' % (NetID)
        #cursor.execute(q)
        #tmp=cursor.fetchall()
        #result={"SiteName":tmp[0][0], "errors":[]}
        #return json.dumps(result)

        if output_format == 'text/html':
            return env.get_template('favorites-tmpl.html').render(
                #change variables
                sites=result,
                info=self.xtra, #this is where xtra is used
                base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'href': '/NetID%s' % (NetID),
                    'SiteName': SiteName,
                    } for SiteName in result]
            return json.dumps(data, encoding='utf-8')

    def DELETE(self,SiteID):
        print "I AM HERE"
	''' Delete favorites with SiteID'''
	#SiteID = cherrypy.request.json["SiteID"] 
	cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='sslp',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"  
        cursor.execute(q)
        q='select exists(select 1 from Site_Favorites where SiteID="%s")' % (SiteID)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
            return errorJSON(code=9004, message="Favorites for SiteID %s Does Not Exist" % (SiteID))
        try:
            q='delete from Site_Favorites where SiteID="%s"' % (SiteID)
            cursor.execute(q)
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9005, message="Failed to delete site from favorites")
        result={"errors":[]}
        return json.dumps(result)

    @cherrypy.tools.json_in(force=False)
    def PUT(self):
        ''' Add or update an item to an order 
        quantity is received in a JSON dictionary
        output is also returned in a JSON dictionary''' 	
        NetID = "abcd"
	SiteID = cherrypy.request.json["SiteID"]
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='sslp',charset='utf8mb4') 
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"
        cursor.execute(q)
        # does orderID exist?
        q='select exists(select 1 from SSLP_Sites where SiteID="%s")' % SiteID
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #orderID does not exist
            return errorJSON(code=9000, message="Site with SiteID %s Does NotExist") % SiteID
        q='select exists(select 1 from Site_Favorites where NDemail="%s" and SiteID="%s")' % (NetID, SiteID)
        cursor.execute(q)
        if cursor.fetchall()[0][0]:
            return errorJSON(code=9001, message="Site with SiteID %s in Favorites already exists") % SiteID
        q='insert into Site_Favorites (NDemail, SiteID) values ("%s", "%s")' % (NetID, SiteID)
        print q
	cursor.execute(q)
	cnx.commit()
	cnx.close()
	result = {'NDemail':NetID, 'SiteID':SiteID, 'errors':[]}
	print result
        return json.dumps(result)

application = cherrypy.Application(Favorites(), None, conf)

