### What is this repository for? ###

* This repository is for the CSE 40613 Web Applications class at Notre Dame. This is an OPEN SOURCE project creating filtering functionality for the Summer Service Learning Program (SSLP) website to enable easier searching for relevant sites.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This section is irrelevant for now. 

* Summary of set up: 
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Not valid

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

*This is the property of Josh Sy and Karina Dube

* Repo owner or admin
* Other community or team contact