#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################


#orderitems
dropTableQuery = ("DROP TABLE IF EXISTS orderitems")
cursor.execute(dropTableQuery)
#options
dropTableQuery = ("DROP TABLE IF EXISTS options")
cursor.execute(dropTableQuery)
#text
dropTableQuery = ("DROP TABLE IF EXISTS text")
cursor.execute(dropTableQuery)
#item
dropTableQuery = ("DROP TABLE IF EXISTS item")
cursor.execute(dropTableQuery)
#menu_sections
dropTableQuery = ("DROP TABLE IF EXISTS menu_sections")
cursor.execute(dropTableQuery)
#shopping_cart
dropTableQuery = ("DROP TABLE IF EXISTS shopping_cart")
cursor.execute(dropTableQuery)
#user
dropTableQuery = ("DROP TABLE IF EXISTS user")
cursor.execute(dropTableQuery)
#menu
dropTableQuery = ("DROP TABLE IF EXISTS menu")
cursor.execute(dropTableQuery)
#Hours
dropTableQuery = ("DROP TABLE IF EXISTS hours")
cursor.execute(dropTableQuery)
#Restaurants
dropTableQuery = ("DROP TABLE IF EXISTS restaurants")
cursor.execute(dropTableQuery)


########################
## Create tables next ##
########################


#Restaurants
createTableQuery = ('''CREATE TABLE restaurants (
						RestID VARCHAR(20) NOT NULL,
						name VARCHAR(45) NOT NULL,
						address VARCHAR(100) NOT NULL,
						city VARCHAR(45) NOT NULL,
						state VARCHAR(20) NOT NULL,
						zip VARCHAR(10) NOT NULL,
						phone VARCHAR(20) NOT NULL,
						lat DECIMAL(10,8) NOT NULL,
						lng DECIMAL(11,8) NOT NULL,
                                                url VARCHAR(100),
						PRIMARY KEY (RestID))'''
                    )
cursor.execute(createTableQuery)

#Hours
createTableQuery = ('''CREATE TABLE hours (
						RestID VARCHAR(20) NOT NULL,
                                                day enum ('M','T','W','TH','F','S','SU') NOT NULL,
                                                open TIME NOT NULL,
                                                close TIME NOT NULL,
                                                PRIMARY KEY(RestID,day,open),
                                                FOREIGN KEY(RestID)
                                                REFERENCES restaurants(RestID)
                                                ON DELETE CASCADE
                                          );'''
                    )
cursor.execute(createTableQuery)

#menu

createTableQuery = ('''CREATE TABLE IF NOT EXISTS menu (
	menuID INT(3) NOT NULL AUTO_INCREMENT,
	RestID VARCHAR(20) NOT NULL,
	category VARCHAR(45) NOT NULL,
	INDEX fk_menu_restaurants1_idx (RestID ASC),
	PRIMARY KEY (menuID),
	CONSTRAINT fk_menu_restaurants1
	FOREIGN KEY (RestID)
 	REFERENCES restaurants (RestID)
  	ON DELETE NO ACTION
  	ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#user

createTableQuery = ('''CREATE TABLE IF NOT EXISTS user (
	firstname VARCHAR(20) NOT NULL,
	lastname VARCHAR(20) NOT NULL,
 	address VARCHAR(100) NOT NULL,
 	city VARCHAR(45) NOT NULL,
 	state VARCHAR(20) NOT NULL,
 	zipcode VARCHAR(10) NOT NULL,
 	phone VARCHAR(20) NOT NULL,
 	email VARCHAR(45) NOT NULL,
	password VARCHAR(120) NOT NULL,
 	PRIMARY KEY (email))
	;''')
cursor.execute(createTableQuery)

#shopping_cart

createTableQuery = ('''CREATE TABLE IF NOT EXISTS shopping_cart (
	order_number INT(11) NOT NULL AUTO_INCREMENT,
  	total_price DECIMAL(6,2) NOT NULL,
  	email VARCHAR(45) NOT NULL,
  	PRIMARY KEY (order_number),
  	FOREIGN KEY (email)
  	REFERENCES user (email)
  	ON DELETE NO ACTION
  	ON UPDATE NO ACTION)
	;''')
cursor.execute(createTableQuery)

#menu sections

createTableQuery = ('''CREATE TABLE IF NOT EXISTS menu_sections (
  sectionID INT(3) NOT NULL AUTO_INCREMENT,
  menuID INT(3) NOT NULL, 
  section VARCHAR(45) NOT NULL,
  PRIMARY KEY (sectionID),
  CONSTRAINT fk_menu_sections_menu1
    FOREIGN KEY (menuID)
    REFERENCES menu (menuID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#item

createTableQuery = ('''CREATE TABLE IF NOT EXISTS item (
  itemID INT(3) NOT NULL AUTO_INCREMENT,
  item_name VARCHAR(45) NULL,
  description VARCHAR(200) NULL,
  price DECIMAL(6,2) NULL,
  sectionID INT(3) NOT NULL,
  PRIMARY KEY (itemID),
  CONSTRAINT fk_item_menu_sections1
    FOREIGN KEY (sectionID)
    REFERENCES menu_sections (sectionID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#text

createTableQuery = ('''CREATE TABLE IF NOT EXISTS text (
  textID INT(3) NOT NULL AUTO_INCREMENT,
  section_text VARCHAR(200) NOT NULL,
  sectionID INT(3) NOT NULL,
  PRIMARY KEY (textID),
  CONSTRAINT fk_text_menu_sections1
    FOREIGN KEY (sectionID)
    REFERENCES menu_sections (sectionID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#options

createTableQuery = ('''CREATE TABLE IF NOT EXISTS options (
  optionID INT(3) NOT NULL AUTO_INCREMENT,
  option_name VARCHAR(45) NOT NULL,
  itemID INT(3) NOT NULL,
  PRIMARY KEY (optionID),
  CONSTRAINT fk_options_item1
    FOREIGN KEY (itemID)
    REFERENCES item (itemID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#orderitems

createTableQuery = ('''CREATE TABLE IF NOT EXISTS orderitems (
  orderitemsID INT(11) NOT NULL AUTO_INCREMENT,
  quantity INT(3) NOT NULL,
  order_number INT(11) NOT NULL,
  itemID INT(3) NOT NULL,
  PRIMARY KEY (orderitemsID),
  CONSTRAINT fk_orderitems_shopping_cart1
    FOREIGN KEY (order_number)
    REFERENCES shopping_cart (order_number)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_orderitems_item1
    FOREIGN KEY (itemID)
    REFERENCES item (itemID)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;''')
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
