#Use this script to populate the |restaurant| table of the database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()


#Loop through the restaurants and add info and menu to database

cursor.execute("delete from options")
cursor.execute("delete from text")
cursor.execute("delete from item")
cursor.execute("delete from menu_sections")
cursor.execute("delete from menu")
cursor.execute("delete from hours")
cursor.execute("delete from restaurants")

for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'RestID' : key,
		'name' : restaurant.get('name'),
		'address' : restaurant.get('street_address'),
		'city' : restaurant.get('locality'),
		'state' : restaurant.get('region'),
		'zip' : restaurant.get('postal_code'),
		'phone' : restaurant.get('phone'),
		'lat' : restaurant.get('lat'),
		'lng' : restaurant.get('long'),
		'url' : restaurant.get('website_url')
	}
	
	addRestaurant = ("INSERT INTO restaurants (RestID, name, address, city, state, zip, phone, lat, lng, url) VALUES (%(RestID)s,  %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s)")
	cursor.execute(addRestaurant,inputDict) 
	for menu in restaurant.get('menus'):
		inputDict = {
			'RestID': key,
			'category': menu.get('menu_name')
		}
		addMenu = ("INSERT INTO menu (RestID, category) VALUES (%(RestID)s, %(category)s)")
        	cursor.execute(addMenu,inputDict)
		menuID = cursor.lastrowid

		for section in menu.get('sections'):
			inputDict = {
			'section' : section.get('section_name'),
			'menuID' : menuID
			}			
			addMenu_Sections = ("INSERT INTO menu_sections (section, menuID) VALUES (%(section)s, %(menuID)s)")
        		cursor.execute(addMenu_Sections,inputDict)
			sectionID = cursor.lastrowid

			for subsection in section.get('subsections'):
				for content in subsection.get('contents'):
					inputDict = {
					'item_name' : content.get('name'),
					'description' : content.get('description'),
					'price' : content.get('price'),
					'section_text' : content.get('text'),
					'type': content.get('type'),
					'sectionID': sectionID
					}	
					if inputDict['type'] == 'SECTION_TEXT':	
				        	addText = ("INSERT INTO text (section_text, sectionID) VALUES (%(section_text)s, %(sectionID)s)")	
        					cursor.execute(addText,inputDict)

					elif inputDict['type'] == 'ITEM':
       						addItem = ("INSERT INTO item (item_name, description, price, sectionID) VALUES (%(item_name)s, %(description)s, %(price)s, %(sectionID)s)")
       						cursor.execute(addItem,inputDict)
						itemID = cursor.lastrowid
						
						if content.get('option_groups'):
							for option in content.get('option_groups'):
								inputDict = {
								'option_name' : option.get('text'),	
								'itemID': itemID
								}
								#print inputDict
        							addOptions = ("INSERT INTO options (option_name, itemID) VALUES (%(option_name)s, %(itemID)s)")
        							cursor.execute(addOptions,inputDict)
	dayDict = {'Monday': 'M', 'Tuesday': 'T', 'Wednesday': 'W', 'Thursday':'TH', 'Friday': 'F', 'Saturday': 'S', 'Sunday': 'SU'
	}
	for day, value in restaurant.get('open_hours').items():
		for hours in value:
			inputDict = {
				'RestID' : key,
				'day' : dayDict[day], 
				'open' : hours.split(' - ')[0],
 				'close' : hours.split(' - ')[1]
			}				
        		addHours = ("INSERT INTO hours (RestID, day, open, close) VALUES (%(RestID)s,  %(day)s, %(open)s, %(close)s)")
        		cursor.execute(addHours,inputDict)
	

#Insert this info into the database        


#Insert hours (hardcoded) for some restaurants
#addHours = ("insert into hours (restId, day, open, close) values ('0559f40309a19b3949d5','M','11:00:00','21:00:00')")
#cursor.execute(addHours)	

cnx.commit()
cnx.close()

