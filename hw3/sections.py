''' Implements handler for /sections
Imported from handler for /restaurants/{id} '''

import os, os.path, json, logging, mysql.connector

import cherrypy
from jinja2 import Environment, FileSystemLoader

from sectionid import SectionID 

env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Sections(object):
    ''' Handles resources /sections/{restID}
        Allowed methods: GET, POST, PUT, DELETE '''
    exposed = True

    def __init__(self):
	self.id=SectionID()
        self.db=dict()
        self.db['name']='lectures'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self, menuID):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
        qn='select category from menu where menuID="%s"' % menuID
        cursor.execute(qn)
        menuName=cursor.fetchone()[0]
        q='select sectionID, section from menu_sections where menuID="%s" order by section' % menuID
        cursor.execute(q)
        result=cursor.fetchall()
        return menuName,result

    def GET(self, restID, menuID):
        ''' Return list of sections for restaurant restID'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            menuName,result=self.getDataFromDB(menuID)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('sections-tmpl.html').render(
                rID=restID,
		mID=menuID,
                mName=menuName,
                sections=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            data = [{
                'href': 'restaurants/%s/menus/%s/sections/%s/items' % (restID, menuID, sectionID),
                'name': section
            } for sectionID, section in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, **kwargs):
        result= "POST /restaurants/{restID}/menus/{menuID}/sections     ...     Sections.POST\n"
        result+= "POST /restaurants/{restID}/menus/{menuID}/sections body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restID, menuID):
        return "<p>/restaurants/{restID}/menus/{menuID}/sections/ allows GET, POST, and OPTIONS</p>"
