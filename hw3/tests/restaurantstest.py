import requests
s = requests.Session()
host='http://localhost'   #  replace by your host here
s.headers.update({'Accept': 'application/json', 'Content-Type':'application/json'})
r = s.get('http://localhost/restaurants',)
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.post('http://localhost/restaurants', data=({"RestID":"a1b2c3d4", "name":"Steak N Shake"}))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)


r = s.get('http://localhost/restaurants/0559f40309a19b3949d5')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.get('http://localhost/restaurants/0559f40309a19b3949d5/menus')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())


r = s.get('http://localhost/restaurants/0559f40309a19b3949d5/menus/2')
print '\n', r.status_code, r.text


r = s.get('http://localhost/restaurants/0559f40309a19b3949d5/menus/2/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.get('http://localhost/restaurants/0559f40309a19b3949d5/menus/2/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.put('http://localhost/orders/2/items/19', data='{"quantity":"5"}')
print '\n', r.status_code 
if r.status_code == requests.codes.ok:
    print(r.json()) 
