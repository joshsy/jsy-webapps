/* Wait until the DOM loads by wrapping our code in a callback to $. */
$(function() {
	
	var $menus = $('.restaurant-list');
  
  $('.refresh').click(function(event) {

    /* Prevent the default link navigation behavior. */
    event.preventDefault();

    /* Get the category JSON data via Ajax. */
    $.ajax({
      type: 'GET',
      url: document.URL,
      dataType: 'json'
    }).done(function(data) {
	console.log(data)


      /* Empty out existing contents in the category list. */
      $menus.empty();

      /* Add a list item/link for each category received. */
      var menus = data;
      for(var i = 0, n = menus.length; i < n; ++i) {
        var menu = menus[i];
	console.log(menu)
	$menus.append(
          $('<a>')
            .addClass('list-group-item')
            .attr('href', menu.href)
            .append(
              $('<h4>')
                .text(menu.name)
                .addClass('list-group-item-heading')
            )
        );
      }
    });
  });
});
