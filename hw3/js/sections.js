/* Wait until the DOM loads by wrapping our code in a callback to $. */
$(function() {
	
	var $sections = $('.restaurant-list');
  
  $('.refresh').click(function(event) {

    /* Prevent the default link navigation behavior. */
    event.preventDefault();

    /* Get the category JSON data via Ajax. */
    $.ajax({
      type: 'GET',
      url: document.URL,
      dataType: 'json'
    }).done(function(data) {
	console.log(data)


      /* Empty out existing contents in the category list. */
      $sections.empty();

      /* Add a list item/link for each category received. */
      var sections = data;
      for(var i = 0, n = sections.length; i < n; ++i) {
        var section = sections[i];
	console.log(section)
	$sections.append(
          $('<a>')
            .addClass('list-group-item')
            .attr('href', section.href)
            .append(
              $('<h4>')
                .text(section.name)
                .addClass('list-group-item-heading')
            )
        );
      }
    });
  });
});
