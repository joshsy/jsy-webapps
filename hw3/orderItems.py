''' Implements handler for /orders/{orderID}/items/{itemID}
To add an item to an order '''
import apiutil
import cherrypy
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON,  SESSION_KEY
from config import conf

class Orders(object):
    ''' Handles resource /orders '''
    exposed = True

    def __init__(self):
	self.id=OrderID()

    def _cp_dispatch(self,vpath):
        print "Orders._cp_dispatch with vpath: %s \n" % vpath
        if len(vpath) == 1: # /orders/{orderID}
            cherrypy.request.params['orderID']=vpath.pop(0)
            return self.id
        if len(vpath) == 2: # /orders/{orderID}/items/
            cherrypy.request.params['orderID']=vpath.pop(0)
            vpath.pop(0) # items
            return self.id.items 
        if len(vpath) == 3: # /orders/{orderID}/items/{itemID}
            cherrypy.request.params['orderID']=vpath.pop(0)
            vpath.pop(0) # items
            cherrypy.request.params['itemID']=vpath.pop(0)
            return self.id.items.id
        return vpath

class OrderID(object):
    exposed = True

    def __init__(self):
	self.items=OrderItems()
#
#    def GET(self, orderID):
#        ''' Return information on order orderID'''
#        return "GET /orders/{id=%s}   ...   OrderID.GET" % orderID
#
#    def PUT(self, orderID, **kwargs):
#        ''' Update order with orderID'''
#        result = "PUT /orders/{id=%s}      ...     OrderID.PUT\n" % orderID
#        result += "PUT /orders body:\n"
#        for key, value in kwargs.items():
#            result+= "%s = %s \n" % (key,value)
#        # Validate form data
#        # Insert or update order
#        # Prepare response
#        return result
#
#    def DELETE(self, orderID):
#        ''' Delete order with orderID'''
#        #Validate orderID
#        #Delete order
#        #Prepare response
#        return "DELETE /orders/{id=%s}   ...   OrderID.DELETE" % orderID
#
#    def OPTIONS(self, orderID):
#        ''' Allows GET, PUT, DELETE, OPTIONS '''
#        #Prepare response
#        return "<p>/orders/{id} allows GET, PUT, DELETE, and OPTIONS</p>"

class OrderItems(object):
    exposed = True

    def __init__(self):
	self.id = OrderItemID()

class OrderItemID(object):
    exposed = True

    def GET(self, orderID, itemID):
        ''' GET orderitemsID, quantity for order_number and itemID or error if no entry with orderID or itemID'''
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='lectures',charset='utf8mb4')        
        cursor = cnx.cursor()
        q='select exists(select 1 from orderitems where order_number="%s" and itemID="%s")' % (orderID, itemID)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
            return errorJSON(code=9004, message="OrderItemID for OrderID %s  and ItemID %s Does Not Exist" % (orderID,itemID))
        q='select orderitemsID, quantity from orderitems where order_number="%s" and itemID="%s"' % (orderID, itemID)
        cursor.execute(q)
        tmp=cursor.fetchall()
        result={"orderitemsID":tmp[0][0],"quantity":tmp[0][1],"errors":[]}
        return json.dumps(result)

        if output_format == 'text/html':
            return env.get_template('orderitems-tmpl.html').render(
                oID=orderID,
                iID=itemID,
		orderName=order_number,
                items=result,
                base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'href': '/orders/%s/items/%s' % (orderID, itemID),
                    'orderID': order_number,
                    'quantity': quantity ,
                    } for itemID, item_name, description, price in result]
            return json.dumps(data, encoding='utf-8')

    def DELETE(self, orderID, itemID):
        ''' Delete orderitem with order_number and itemID'''
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='lectures',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"  
        cursor.execute(q)
        q='select exists(select 1 from orderitems where order_number="%s" and itemID="%s")' % (orderID, itemID)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemID for this orderID / itemID combination
            return errorJSON(code=9004, message="OrderItemID for OrderID %s  and ItemID %s Does Not Exist" % (orderID,itemID))
        try:
            q='delete from orderitems where order_number="%s" and itemID="%s"' % (orderID, itemID)
            cursor.execute(q)
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9005, message="Failed to delete order item from shopping cart")
        result={"errors":[]}
        return json.dumps(result)


    @cherrypy.tools.json_in(force=False)
    def PUT(self, orderID, itemID):
        ''' Add or update an item to an order 
        quantity is received in a JSON dictionary
        output is also returned in a JSON dictionary''' 
        try:
            quantity = int(cherrypy.request.json["quantity"])
            print "quantity received: %s" % quantity
        except:
            print "quantity was not received"
            return errorJSON(code=9003, message="Expected integer 'quantity' of items in order as JSON input")
        sess = cherrypy.session

	# manually regenerate session
	sess.regenerate()
	sess['_cp_username'] = cherrypy.request.login
	# TODO: remove manual regeneration of session

        username = "abcd@nd.edu"
        print 'order items SESSION KEY "%s"' % SESSION_KEY
        print 'value of SESSION KEY "%s"' % cherrypy.session[SESSION_KEY]
        if username:
            print 'found username "%s" of session "%s"' % (username, SESSION_KEY)

        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='lectures',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"  
        cursor.execute(q)
        # does orderID exist?
        q='select exists(select 1 from shopping_cart where order_number="%s")' % orderID
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #orderID does not exist
	    q='insert into shopping_cart (user_email) values ("%s")' % (username)
	    cursor.execute(q)
	    qn='select order_number from shopping_cart where user_email="%s"' % (username)
	    cursor.execute(qn)
	    orderID=cursor.fetchone()[0]
	    cursor.fetchall()
        # does itemID exist?
        q='select exists(select 1 from item where itemID="%s")' % itemID
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #itemID does not exist
            return errorJSON(code=9001, message="Item with ItemID %s Does Not Exist") % itemID
        q='insert into orderitems (order_number, itemID, quantity) values (%s, %s, %s) on duplicate key update quantity=%s;' % (orderID, itemID, quantity, quantity)
        cursor.execute(q)
        q="select orderitemsID from orderitems where order_number=%s and itemID=%s" % (orderID,itemID)
        orderItemID=0
        try:
            cursor.execute(q)
            orderItemID=cursor.fetchall()[0][0]
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9002, message="Failed to add order item to shopping cart")
        result = {'orderitemsID':orderItemID, 'order_number':orderID, 'itemID':itemID, 'quantity':quantity, 'errors':[]}
        return json.dumps(result)

application = cherrypy.Application(Orders(), None, conf)

