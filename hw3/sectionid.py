''' Implements handler for /sections/{id}
Imported from handler for /sections'''
import cherrypy
from items import Items
class SectionID(object):
    ''' Handles resource /sections/{restID}/{sectionID}
        Allowed methods: GET, PUT, DELETE '''
    exposed = True

    def __init__(self):
        self.items = Items()

    def GET(self, restID, menuID, sectionID):
        ''' Return info of category id for restaurant id'''
        return "GET /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectionID=%s}  ...   SectionID.GET" % (restID,menuID, sectionID)

    def PUT(self, restID, menuID, sectionID, **kwargs):
        ''' Update category id for restaurant id'''
        result = "PUT /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectionID=%s}   ...   SectionID.PUT\n" % (restID,menuID, sectionID)
        result += "PUT /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectionID=%s} body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restID, menuID, sectionID):
        #Validate id
        #Delete restaurant
        #Prepare response
        return "DELETE /restaurants/{restID=%s}/menus/{menuID=%s}/sections/{sectionID=%s}   ...   SectionID.DELETE" % (restID,menuID, sectionID)

    def OPTIONS(self,restID, menuID, sectionID):
        return "<p>/restaurants/{restID}/menus/{menuID}/sections/{sectionID} allows GET, PUT, DELETE, and OPTIONS</p>"

